<?php
///**
// * Created by PhpStorm.
// * User: rsanjose
// * Date: 17/06/2019
// * Time: 1:07 PM
// */
//
//class PersonTransactionTest extends TestCase
//{
//    public function testGetPersonProfile()
//    {
//        $pumpData = [
//            'first_name' => 'Bruce',
//            'last_name' => 'Lee',
//            'mobile_number' => 00000000000,
//            'phone_number' => 0000000,
//            'permanent_address' => 'sample address 1',
//            'current_address' => 'sample current address'
//        ];
//        $stubPerson = $this->getMockBuilder(\App\Core\Domain\Repository\PersonRepositoryInterface::class)
//            ->disableOriginalConstructor()
//            ->setMethods(['find', 'findBy', 'all', 'commit', 'transact',
//                'save',  'update', 'delete'])
//            ->getMock();
//        $stubPerson->method("find")
//            ->willReturn($pumpData);
//        $personTxn = new \App\Core\Domain\UseCase\PersonTransaction($stubPerson,
//            new \App\Core\Domain\Entity\Person(), 'PersonDecorator');
//        $request = new \App\Core\Domain\Boundery\Request(["id" => 1]);
//        $request->setData(["id" => 1]);
//        $expectedValue = array_merge($pumpData, [
//            'status' => null,
//            'delete' => null,
//            'created_by' => null,
//            'updated_by' => null,
//            'created_at' => null,
//            'updated_at' => null,
//        ]);
//        $result = $personTxn->findSingleData($request);
//        $this->assertTrue($result != null);
//
//        $this->assertEquals($expectedValue, $result);
//    }
//}