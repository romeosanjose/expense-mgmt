<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 21/06/2019
 * Time: 4:22 PM
 */

class AddUserTest extends TestCase
{
    /**
     * @dataProvider dataForUserCreate
     */
    public function testCreateUserProfile($data, $expectedResult)
    {
        $mockRepo = $this->getMockBuilder(\App\Core\Domain\Repository\UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockRepo->method("store")
            ->willReturn([]);
        $request = new \App\Core\Domain\Boundery\Request($data);
        $userTxn = new \App\Core\Domain\UseCase\AddUser($mockRepo);
        try{
            $result = $userTxn->addUser($request);
        }catch(\App\Core\Domain\Validator\ValidationException $e) {
            $this->assertEquals($expectedResult , $e->getMessage());
        }
    }

    public function dataForUserCreate()
    {
        return [
            [
                [
                    'username' => 'batman',
                    'password' => '@password',
                    'email'    => 'batman@hero.com',
                ],
                '["password field should be same as password_verify","password_verify field is required"]'
            ]
        ];
    }


}