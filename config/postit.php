<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/08/2019
 * Time: 6:35 AM
 */


return [
    'repositories' => [
        \App\Domain\User\Repository\UserRepository::class => \App\Domain\User\Repository\UserDoctrineRepository::class,
        \App\Domain\User\Repository\PersonRepository::class => \App\Domain\User\Repository\PersonDoctrineRepository::class,
    ],

    'permissions' => [
        'ADMIN' => [
            'user' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'person' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'patient' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'post' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'event' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'template' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'file' => ['POST','GET', 'PATCH', 'PUT']
        ],
        'PUBLISHER' => [
            'user' => ['GET'],
            'person' => ['GET'],
            'patient' => ['POST','GET', 'PATCH', 'PUT'],
            'post' => ['POST','GET', 'PATCH', 'PUT'],
            'file' => ['POST','GET', 'PATCH', 'PUT']
        ]
    ],

    'own_permission' => [
        'user',
        'person',
    ],

    'storage' => [
        'profile_picture' => 'profile_picture'
    ],

];
