<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 18/09/2019
 * Time: 9:47 PM
 */

namespace App\Domain\User\UseCase;


use App\Domain\Core\Boundery\Request;
use App\Domain\File\File;
use App\Domain\User\Person;
use App\Domain\User\User;


class UpdatePerson extends AddPerson
{
    public function updatePerson( Request $request, $file, User $updateBy)
    {

        $personData = $request->getData();
        $person = $this->getRecord($personData['id']);
        unset($personData['file']);
        $person = $person->fromArray($personData);
        if (!is_null($file)){
            $person->setProfilePicture($file);
        }
        $person  = $this->setEditor($person, $updateBy);

        return $this->repository->update($person, Person::class);
    }
}
