<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 23/09/2019
 * Time: 1:18 PM
 */

namespace App\Domain\User\UseCase;

use App\Domain\Core\Boundery\Request;
use App\Domain\Core\Boundery\Response;
use App\Domain\Core\UseCase\UseCase;

class ChangePassword extends UseCase
{
    public function changePassword(Request $request)
    {
        if ($this->repository->updateBy(['password' => $request->get('password')], ['id' => $request->get('id')])) {
            $response = new Response('Successfully change password!');
            $response->setCode(200);
        } else {
            $response = new Response('unable to change password');
            $response->setCode(501);
        }

        return $response;
    }
}
