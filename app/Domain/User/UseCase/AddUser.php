<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/06/2019
 * Time: 8:27 AM
 */

namespace App\Domain\User\UseCase;

use App\Domain\Core\Boundery\Request;
use App\Domain\Core\Boundery\Response;
use App\Domain\Core\UseCase\UseCase;
use App\Domain\User\Decorator\UserProtectedDecorator;
use App\Domain\User\Events\UserCreated;
use App\Domain\User\User;
use App\Events\Event;


class AddUser extends UseCase
{
    protected $validationRules = [
        'username' => [
            'required',
            'max_length(50)'
        ],
        'email' => [
            'required',
            'email'
        ],
        'password' => [
            'required',
            'equals(:password_verify)'
        ],
        'password_verify' => [
            'required'
        ],
        'type' => [
            'required'
        ]
    ];

    /**
     * @param Request $request
     * @param User $createdBy
     * @return Response|mixed
     */
    public function addUser(Request $request, User $createdBy)
    {
        $data = $request->getData();
        $this->validateUserData($data);
        if ($this->isUsernameExists($data['username'])) {
            return $this->response('username already exists!', 409);
        }

        $user = (new User())->fromArray($data);
        $user = $this->encryptUserPassword($user, $data['password']);
        $user->setApiKey(bin2hex(openssl_random_pseudo_bytes(64)));
        unset($data['password']);
        $response = $this->addNewRecord(new Request($data), $user, $createdBy, new UserProtectedDecorator(), $validated = true);


        return $response;
    }

    private function encryptUserPassword(User $user, $password)
    {
        $user->setSalt(sha1($password));
        $user->setPassword(sha1($password . $user->getSalt()));

        return $user;
    }

    private function isUsernameExists($username)
    {
        $userObjs = $this->repository->findBy(['username' => $username]);
        if (count($userObjs) > 0) {
            return true;
        }

        return false;
    }
}
