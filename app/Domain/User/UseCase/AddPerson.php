<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/06/2019
 * Time: 12:28 PM
 */

namespace App\Domain\User\UseCase;


use App\Domain\Core\Boundery\Request;
use App\Domain\Core\Boundery\Response;
use App\Domain\Core\UseCase\UseCase;
use App\Domain\User\Decorator\PersonDecorator;


class AddPerson extends UseCase
{
    protected $validationRules = [
        'first_name' => [
            'required',
            'max_length(50)'
        ],
        'last_name' => [
            'required',
            'max_length(50)'
        ],

    ];

}
