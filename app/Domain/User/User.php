<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:17 PM
 */


namespace App\Domain\User;


use App\Domain\Core\Entity\AbstractEntity;
use App\Domain\Core\Entity\Auditable;
use App\Domain\Core\Entity\AuditableTrait;
use App\Domain\Core\Entity\Identity;
use App\Domain\Core\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package App\Core\User
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends AbstractEntity implements Identity, Auditable
{
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $log_attempt = 0;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $locked = false;
    /**
    * @ORM\OneToOne(targetEntity="App\Core\User\Person")
    * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
    **/
    private $person;

    /**
     * @ORM\Column(type="string")
     */
    private $api_key;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Core\User\User", inversedBy="followers")
     * @ORM\JoinTable(name="link_followers",
     * joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="following_user_id", referencedColumnName="id")}
     * )
     */
    private $following;

    /**
     * @ORM\ManyToMany(targetEntity="App\Core\User\User", mappedBy="following")
     **/
    private $followers;


    /**
    * @ORM\OneToOne(targetEntity="App\Core\Setting\ManageList")
    * @ORM\JoinColumn(name="security_id", referencedColumnName="id")
    **/
    private $security_question;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $security_answer;

    public function __construct()
    {
        $this->following = new ArrayCollection();
        $this->followers = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function entityProperties()
    {
        return [
            'username',
            'email',
            'log_attempt',
            'locked',
            'person',
            'following',
            'followers',
            'type',
            'security_question',
            'security_answer'
        ];
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogAttempt()
    {
        return $this->log_attempt;
    }

    /**
     * @param mixed $logAttempt
     */
    public function setLogAttempt($logAttempt)
    {
        $this->log_attempt = $logAttempt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked(bool $locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public function encryptPassword($password, $salt)
    {
        return sha1($password . $salt);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param mixed $api_key
     */
    public function setApiKey($api_key): void
    {
        $this->api_key = $api_key;
    }

    /**
     * @return mixed
     */
    public function getFollowing(): Collection
    {
//        dd($this->following);
        return $this->following;
    }

    /**
     * @param mixed $following
     */
    public function follow(User $user): void
    {
        $this->following[] = $user;
        $user->followedBy($this);
    }

    /**
     * @param User $user
     */
    public function unfollow(User $user) : void
    {
        $this->following->removeElement($user);
        $this->unfollowedBy($this);
    }

    /**
     * @return mixed
     */
    public function getFollowers() : Collection
    {
        return $this->followers;
    }

    /**
     * @param mixed $followers
     */
    private function followedBy(User $user): void
    {
        $this->followers[] = $user;
    }

    /**
     * @param User $user
     */
    private function unfollowedBy(User $user) : void
    {
        $this->followers->removeElement($user);
    }

    /**
    * @return mixed 
    */
    public function getSecurityQuestion()
    {
        return $this->security_question;
    }
    
    /**
     * @param mixed $security_question
     */
    public function setSecurityQuestion($security_question)
    {
        $this->security_question = $security_question;
    }

    /**
    * @return mixed 
    */
    public function getSecurityAnswer()
    {
        return $this->security_answer;
    }

    /**
     * @param mixed $security_answer
     */
    public function setSecurityAnswer($security_answer)
    {
        $this->security_answer = $security_answer;
    }

    public function authenticate($email, $password)
    {
        $isValid = false;
        if ($email === $this->getEmail()){
            if ($this->encryptPassword($password, $this->getSalt()) === $this->getPassword()){
                $isValid = true;
            }
        }

        return $isValid;
    }
}
