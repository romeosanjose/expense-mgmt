<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 08/08/2019
 * Time: 2:42 PM
 */

namespace App\Domain\User\Repository;


use App\Domain\Core\Repository\Doctrine\DoctrineRepository;
use App\Domain\User\User;
use App\Domain\User\Repository\UserRepository;


class UserDoctrineRepository extends DoctrineRepository implements UserRepository
{
    public function entity()
    {
        return User::class;
    }
}
