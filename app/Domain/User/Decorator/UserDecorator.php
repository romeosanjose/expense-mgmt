<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 28/06/2019
 * Time: 11:33 AM
 */

namespace App\Domain\User\Decorator;


use App\Domain\Core\Boundery\Decorator\Decorator;
use App\Domain\Core\Entity\AbstractEntity;

class UserDecorator extends Decorator
{
    protected function properties()
    {
        return [
            'id',
            'username',
            'password',
            'api_key',
            'salt',
            'email',
            'log_attempt',
            'locked',
            'person',
            'following',
            'followers',
            'deleted',
            'type',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
        ];
    }


    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($this->properties());
    }
}
