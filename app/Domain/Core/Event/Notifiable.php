<?php


namespace App\Domain\Core\Event;



interface Notifiable
{
    public function getRootEntity();

    public function getEventName();


}
