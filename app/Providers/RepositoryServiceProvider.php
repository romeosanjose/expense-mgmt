<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/08/2019
 * Time: 6:34 AM
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $repositories = $this->app['config']->get('postit.repositories');
        foreach ($repositories as $abstract_repository => $concrete_repository) {
            $this->app->singleton($abstract_repository, $concrete_repository);
        }
    }

}