<?php


namespace App\Http\Controllers;


use App\Core\Domain\UseCase\GeneratePassword;
use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;

class PasswordController extends Controller
{
    public function generatePassword()
    {
        $generatePasswordCommand = new GeneratePassword(new HumanPasswordGenerator());
        return $generatePasswordCommand->generatePassword();
    }
}
