<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 22/08/2019
 * Time: 4:40 PM
 */

namespace App\Http\Controllers;


use App\Core\Domain\Boundery\Request as UseCaseRequest;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\User\Person;
use App\Core\User\Repository\PersonRepository;
use App\Core\User\UseCase\AddPerson;


class PersonController extends Controller
{
    private $personEntity;

    private $personRepository;

    public function __construct(PersonRepository $personRepository, Person $person)
    {
        $this->personEntity = $person;
        $this->personRepository = $personRepository;
    }

    public function storeAction(\Illuminate\Http\Request $request)
    {
        $useCaseRequest = new UseCaseRequest($request->json('person'));
        try {
            $response = (new AddPerson($this->personRepository))->addNewRecord($useCaseRequest, new Person());
            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $e){
            return response($e->getErrors(), 500);
        }

    }
}