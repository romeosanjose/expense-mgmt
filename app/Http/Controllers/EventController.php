<?php


namespace App\Http\Controllers;

use App\Core\Notification\Decorator\NotificationEventDecorator;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\UseCase\GetEvent;

class EventController extends Controller
{
    private $eventRepository;

    public function __construct(NotificationEventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function getAllAction()
    {
        try{
            $getEvent = new GetEvent($this->eventRepository);
            $response = $getEvent->getAllActiveRecords(new NotificationEventDecorator());

            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function getEvent($id)
    {
        try{
            $getEvent = new GetEvent($this->eventRepository);
            $response = $getEvent->getRecord($id, new NotificationEventDecorator());
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

}
