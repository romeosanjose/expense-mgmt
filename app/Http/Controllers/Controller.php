<?php

namespace App\Http\Controllers;

use App\Core\Domain\Boundery\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function setResponse(Response $response)
    {
        return response($response->getData(), $response->getCode());
    }
}
