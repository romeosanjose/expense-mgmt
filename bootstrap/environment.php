<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 5:42 PM
 */

$env = $app->detectEnvironment(function(){
    $environmentPath = __DIR__.'/../.env';
    $setEnv = trim(file_get_contents($environmentPath));
    if (file_exists($environmentPath))
    {
        putenv('APP_ENV='.$setEnv);
        if (getenv('APP_ENV') && file_exists(__DIR__.'/../.' .getenv('APP_ENV') .'.env')) {
            $dotenv = Dotenv\Dotenv::create(__DIR__.'/../', '.'.getenv('APP_ENV').'.env');
            $dotenv->overload();
        }
    }});